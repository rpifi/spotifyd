FROM rust:1.43.0-slim-stretch AS build

COPY ./src/ /usr/src/spotifyd/
WORKDIR /usr/src/spotifyd

RUN apt-get update \
 && apt-get -y install \
    libpulse-dev \
    libssl-dev \
    pkg-config \
 && rm -rf /var/lib/apt/lists/* \
 && cargo build \
    --release \
    --no-default-features \
    --features pulseaudio_backend
    

FROM debian:stretch-slim

COPY --from=build /usr/src/spotifyd/target/release/spotifyd /usr/bin/spotifyd

ENTRYPOINT ["/usr/bin/spotifyd"]
